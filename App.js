import React from "react";
import { SafeAreaView } from 'react-native';

import Header from './src/components/Header'
import Carousel from "./src/components/Carousel";


export default function App(){
  return(
    <SafeAreaView>

      <Header />

      <Carousel />

    </SafeAreaView>
  );
}
