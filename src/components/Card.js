import React from "react";
import {StyleSheet, View} from 'react-native';

export default function Card(props) {
    return (
        <View style={styles.card}>
            <View style={styles.cardContent}>
                { props.children}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 10,
        elevation: 10,
        backgroundColor: 'purple',
        shadowColor: '#000',
        shadowOpacity: 0.3,
        shadowRadius: 2,
        marginHorizontal: 40,
        marginVertical: 40

    },

    cardContent: {
        marginHorizontal: 15,
        marginVertical: 10,

    },

});