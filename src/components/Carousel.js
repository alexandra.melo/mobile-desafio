import React from "react";
import {Image, Text, ScrollView } from 'react-native';

import Card from '../components/Card'

export default function Carousel() {
    return(
        <ScrollView pagingEnabled horizontal>

          <Card>
            <Text> Esse é o modelo de card </Text>
            <Image
            source={require('../assets/cartoon.png')}
            style={{width: 300, height: 300}}
            />
            <Text> A imagem acima é só um teste </Text>
            <Text> Esse é o card 1/4 </Text>
          </Card>

          <Card>
            <Text> As frases são para testar </Text>
            <Image
            source={require('../assets/cartoon.png')}
            style={{width: 300, height: 300}}
            />
            <Text> A imagem acima é só um teste </Text>
            <Text> Esse é o card 2/4 </Text>
          </Card>

          <Card>
            <Text> se o props children funcionou </Text>
            <Image
            source={require('../assets/cartoon.png')}
            style={{width: 300, height: 300}}
            />
            <Text> A imagem acima é só um teste </Text>
            <Text> Esse é o card 3/4 </Text>
          </Card>

          <Card>
            <Text> fim </Text>
            <Image
            source={require('../assets/cartoon.png')}
            style={{width: 300, height: 300}}
            />
            <Text> A imagem acima é só um teste </Text>
            <Text> Esse é o card 4/4 </Text>
          </Card>

      </ScrollView>
    )
}