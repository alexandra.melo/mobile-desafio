import React from "react";
import {StyleSheet, View, Image} from 'react-native';

export default function Header(){
    return(

        <View style={styles.appbar}>

            <View style={styles.header1}>
                <Image
                source={require('../assets/menu.png')}
                style={{width: 35, height: 35}}
                />
            </View>

            <View style={styles.header2}>
                <Image
                source={require('../assets/perfil.png')}
                style={{width: 35, height: 35}}
                />
            </View>
    
        </View>
    )
}

const styles = StyleSheet.create({
    header1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
  
    header2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'flex-end'
  },

    appbar: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        backgroundColor: '#87BADE',
        paddingTop: 30
    },

});